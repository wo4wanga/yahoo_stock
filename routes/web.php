<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/home', function () {
//     return view('test');
// });
// Route::get('test', function () {
//     return view('test');
// });
// Route::get('auth', function () {
//     return view('yconnect.yconnect_php.sampleUserInfo');
// });
// Route::get('login', function () {
//     return view('login');
// });
// Route::get('/test','ViewController@test');
Route::get('/dev/centerwave','ViewController@centerwave');
Route::get('/dev/cwstore','ViewController@cwstore');
Route::get('/show', function () {
    return view('v1login1');
});