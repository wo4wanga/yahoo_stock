<!-- <!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="/js/app.css" rel="stylesheet" type="text/css">
    </head> -->
    <span class="yconnectLogin"></span>

<script type="text/javascript">
window.yconnectInit = function() {
    YAHOO.JP.yconnect.Authorization.init({
        button: {    // ボタンに関しては下記URLを参考に設定してください
                     // https://developer.yahoo.co.jp/yconnect/loginbuttons.html
            format: "image",
            type: "a",
            textType:"a",
            width: 196,
            height: 38,
            className: "yconnectLogin"
        },
        authorization: {
            clientId: "dj00aiZpPVMzSWs4dFRFT3VucyZzPWNvbnN1bWVyc2VjcmV0Jng9NjA-",    // 登録したClient IDを入力してください
            redirectUri: "http://127.0.0.1:8000/dev/centerwave", // 本スクリプトを埋め込むページのURLを入力してください
            scope: "openid",
            responseType: "code",
            state: "44Oq44Ki5YWF44Gr5L+644Gv44Gq44KL77y",
            nonce: "5YOV44Go5aWR57SE44GX44GmSUTljqjjgavjgarjgaPjgabjog=",
            windowWidth: "500",
            windowHeight: "400"
        },
        autofill: {
            // 属性パラメーター: "属性情報を挿入したいフォーム要素のID名"
            // name: "name",
            // email: "email",
            // address: "address"
        },
        onSuccess: function(res) {
            // 正常時のコールバック関数
            // res変数に各属性情報が格納されます
        },
        onError: function(res) {
            // エラー発生時のコールバック関数
        },
        onCancel: function(res) {
            // 同意キャンセルされた時のコールバック関数
        }
    });
};
(function(){
var fs = document.getElementsByTagName("script")[0], s = document.createElement("script");
s.setAttribute("src", "https://s.yimg.jp/images/login/yconnect/auth/2.0.1/auth-min.js");
fs.parentNode.insertBefore(s, fs);
})();
</script>
    
<!-- </html> -->