<?php

class yahoostore {

    function getitem($access_token,$seller_id,$item_code){
        
        //get item
        $header = [
            'GET /ShoppingWebService/V1/getItem?seller_id='.$seller_id.'&item_code='.$item_code.' HTTP/1.1',
            'Host: circus.shopping.yahooapis.jp',
            'Authorization: Bearer ' . $access_token
        ];

        $url = 'https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/getItem?seller_id='.$seller_id.'&item_code='.$item_code;

        // 必要に応じてオプションを追加してください。
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER,     $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if($seller_id=='cwstore'){
            $quandif = 'quantity_dif2';
        } else {
            $quandif = 'quantity_dif1';
        }

        //it-05002->指定された商品コードは存在しません。
        $response = curl_exec($ch);
        curl_close($ch);

        header("Content-type:text/xml;charset=utf-8");
        //取得したXMLを配列に代わる、アイテムを取り出す
        $xml = simplexml_load_string($response,NULL,LIBXML_NOCDATA);
        $array_info=json_decode(json_encode($xml),true);
    

        if(array_key_exists("Code",$array_info)){
            return '該当skuがありません';

        } else {

            $quantity_info = $array_info['Result']['Quantity'];
         
            //print_r(count($quantity_info));
            //echo count($quantity_info);
    
            if(count($quantity_info)==0){
    
                // $subcode = array();
                // $quantity = array();
                $subcode_info = $array_info['Result']['SubCodes']['SubCode'];
    
            for($i=0;$i<count($subcode_info);$i++){
    
                $subcode = $subcode_info[$i]['@attributes']['code'];
                $quantity = $subcode_info[$i]['@attributes']['quantity'];
                $orig_qunt =  DB::table("subsku-quantity_info")->where('subsku',$subcode)->pluck('quantity');
                //数列のため0を付ける
                $qunt_dif = $orig_qunt[0] - $quantity;
                $bool=DB::table("subsku-quantity_info")->where('subsku',$subcode)->update([$quandif=>$qunt_dif]);
            }
    
            } else{
                $quantity = $quantity_info;
                $orig_qunt =  DB::table("sku-quantity_info")->where('sku','=',$item_code)->pluck('quantity');
                $qunt_dif = $orig_qunt[0] - $quantity;
                $bool=DB::table("sku-quantity_info")->where('sku',$item_code)->update([$quandif=>$qunt_dif]);
            }
                return 'アップデートが完了でした';
       }
       
    }
}




// $json_string = json_encode($subcode);

//get itemlist
// $header = [
//     'GET /ShoppingWebService/V1/myItemList?seller_id=centerwave&stock=true&start=1&results=50&query=hdmi&type=name&sort=%2Bitem_code HTTP/1.1',
//     'Host: circus.shopping.yahooapis.jp',
//     'Authorization: Bearer ' . $access_token
// ];

// $url = 'https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/myItemList?seller_id=centerwave&stock=true&start=1&results=50&query=hdmi&type=name&sort=%2Bitem_code';

// // 必要に応じてオプションを追加してください。
// $ch = curl_init($url);
// curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'GET');
// curl_setopt($ch, CURLOPT_HTTPHEADER,     $header);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// $response = curl_exec($ch);

// // preg_match('/<display>(.+?)<\/display>/', $response, $matches);
// // print_r($matches);
// curl_close($ch);
// header("Content-type:text/xml;charset=utf-8");
// //取得したXMLを配列に代わる、アイテムを取り出す
// $xml = simplexml_load_string($response,NULL,LIBXML_NOCDATA);
// $array=json_decode(json_encode($xml),true);
// for( $i=0; $i<count($array['Result'],0); $i++){
//     $item_code[] = $array['Result'][$i];
//     // $name[] = $array['Result'][$i]['Name'];
//     // $original_price[] = $array['Result'][$i]['OriginalPrice'];
//     // $price[] = $array['Result'][$i]['Price'];
//     // $sale_pricep[] = $array['Result'][$i]['SalePrice'];
//     // $quantity[] = $array['Result'][$i]['Quantity'];
// }
// $json_string = json_encode($item_code);
// print_r($json_string);








//get stock
//   $header = [
//       'POST /ShoppingWebService/V1/getStock HTTP/1.1',
//       'Host: circus.shopping.yahooapis.jp',
//       'Authorization: Bearer ' .$access_token
//   ];

//   $url   = 'https://circus.shopping.yahooapis.jp/ShoppingWebService/V1/getStock';
//   $param = array(
//               "seller_id" => 'centerwave',//ストアアカウント
//               "item_code" => '3cm-20190419-cyj2465',//商品コード
//           );


//   // 必要に応じてオプションを追加してください。
//   $ch = curl_init();
//   curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'POST');
//   curl_setopt($ch, CURLOPT_HTTPHEADER,     $header);
//   curl_setopt($ch, CURLOPT_URL,            $url);
//   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//   curl_setopt($ch, CURLOPT_POST,           true);
//   curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($param));

//   $response = curl_exec($ch);
//   print_r($response);
//   curl_close($ch);

?>
    