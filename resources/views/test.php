<?php
require_once('header.php');

?>

<span class="yconnectLogin"></span>

<script type="text/javascript">
window.yconnectInit = function() {
    YAHOO.JP.yconnect.Authorization.init({
        button: {    // ボタンに関しては下記URLを参考に設定してください
                     // https://developer.yahoo.co.jp/yconnect/loginbuttons.html
            format: "image",
            type: "a",
            textType:"a",
            width: 196,
            height: 38,
            className: "yconnectLogin"
        },
        authorization: {
            clientId: "dj00aiZpPWdCUFNkV29aS29rdyZzPWNvbnN1bWVyc2VjcmV0Jng9YzY-",    // 登録したClient IDを入力してください
            redirectUri: "http://127.0.0.1:8000/dev", // 本スクリプトを埋め込むページのURLを入力してください
            scope: "openid",
            // Authorization Codeフローの場合はresponseType, state, nonceパラメーターは必須です,
            responseType: "code",
            state: "44Oq44Ki5YWF44Gr5L+644Gv44Gq44KL77yB",
            nonce: "5YOV44Go5aWR57SE44GX44GmSUTljqjjgavjgarjgaPjgabjgog=",
            windowWidth: "500",
            windowHeight: "400"
        },
        onError: function(res) {
            // エラー発生時のコールバック関数
        },
        onCancel: function(res) {
            // 同意キャンセルされた時のコールバック関数
        }
    });
};
(function(){
var fs = document.getElementsByTagName("script")[0], s = document.createElement("script");
s.setAttribute("src", "https://s.yimg.jp/images/login/yconnect/auth/2.0.1/auth-min.js");
fs.parentNode.insertBefore(s, fs);
})();
</script>